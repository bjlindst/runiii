!! S. Fartoukh. Pedestrain, sample job for using Run III optics files
!! Updated by F.F. Van der Veken


!#######################
!# Sequence and Optics #
!#######################
option,-echo,-warn;

System,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII dbrun3";
System,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII/RunIII_dev/2021_V6 run3opt";
!System,"ln -fns /afs/cern.ch/work/f/fvanderv/projects/CollimationRunIII/Templates run3aper";

REAL CONST l.TAN   = 0.0  ;REAL CONST l.TANAL = l.TAN;
REAL CONST l.TANAR = l.TAN;REAL CONST l.TANC  = l.TAN;
REAL CONST l.TCT   = 1.0;REAL CONST l.TCTH  = l.TCT;REAL CONST l.TCTVA = l.TCT;
REAL CONST l.MBAS2             =0 ; REAL CONST l.MBAW              =0 ;
REAL CONST l.MBCS2             =0 ; REAL CONST l.MBLS2             =0 ;
REAL CONST l.MBLW              =0 ; REAL CONST l.MBWMD             =0 ;
REAL CONST l.MBXWH             =0 ; REAL CONST l.MBXWS             =0 ;
REAL CONST l.MBXWT             =0 ;

mylhcbeam=1;
on_collision=1;


call,file="dbrun3/toolkit/macro.madx";
if (mylhcbeam<4){
  call,file="dbrun3/LHC_LS2_2021-09-15.seq";
  bv_aux=1;
} else {
  call,file="dbrun3/LHC_LS2_2021-09-15_b4.seq";
  bv_aux=-1;
};

if (mylhcbeam<4){
  call,   file="dbrun3/aperture/aperture.b1.madx";
  call,   file="dbrun3/aperture/aper_tol.b1.madx";
};
call,   file="dbrun3/aperture/aperture.b2.madx";
call,   file="dbrun3/aperture/aper_tol.b2.madx";

!! Load some optics
call,file="run3opt/PROTON/opticsfile.32";

!! Save tunes for matching later
qx0=Qxb1;
qy0=Qyb1;
qprime=3;   !qprime=15;  ! Overwrite, because value in optics is 0
I_MO=0;     !I_MO=-300;

!! Set beam
if (mylhcbeam<4){
  Beam,particle=proton,sequence=lhcb1,energy=NRJ,NPART=1.4E11,sige=4.5e-4;          ! NPART=1.15E11
};
Beam,particle=proton,sequence=lhcb2,energy=NRJ,bv=-bv_aux,NPART=1.4E11,sige=4.5e-4; ! NPART=1.15E11

!! Which beta*, which telescope, which corresponding pre-squeezed beta* ?
if (mylhcbeam<4){
  ! beta* in IR1/2/5/8
  value,betxip1b1,betxip2b1,betxip5b1,betxip5b1;
  ! Tele-index in IR1/5
  value,rx_IP1,ry_IP1,rx_IP5,ry_IP5;
  ! Corresponding pre-squeezed beta* at IP1 and IP5
  betxip1b1_pre=betxip1b1*rx_IP1;betyip1b1_pre=betyip1b1*ry_IP1;betxip5b1_pre=betxip5b1*rx_IP5;betyip5b1_pre=betyip5b1*ry_IP5;
  value,betxip1b1_pre,betyip1b1_pre,betxip5b1_pre,betyip5b1_pre;
};
! beta* in IR1/2/5/8
value,betxip1b2,betxip2b2,betxip5b2,betxip5b2;
! Tele-index in IR1/5
value,rx_IP1,ry_IP1,rx_IP5,ry_IP5;
! Corresponding pre-squeezed beta* at IP1 and IP5
betxip1b2_pre=betxip1b2*rx_IP1;betyip1b2_pre=betyip1b2*ry_IP1;betxip5b2_pre=betxip5b2*rx_IP5;betyip5b2_pre=betyip5b2*ry_IP5;
value,betxip1b2_pre,betyip1b2_pre,betxip5b2_pre,betyip5b2_pre;


!! Save crossing scheme
on_x1_aux=on_x1;on_x5_aux=on_x5;on_x2_aux=on_x2;on_x8_aux=on_x8;on_sep1_aux=on_sep1;on_sep2_aux=on_sep2;on_sep5_aux=on_sep5;on_sep8_aux=on_sep8;on_alice_aux=on_alice;on_lhcb_aux=on_lhcb;

!! write out lengths and positions
if (mylhcbeam==1){ use,sequence=lhcb1; } else { use,sequence=lhcb2; };
select,flag=twiss,clear;
select,flag=twiss,column=name,keyword,s,L;
twiss,file="positions_B1.tfs";



!###################
!# Check and Slice #
!###################

print,text="";
print,text="";
print,text="Preparing Beam 1 -----------------------------------------------------";
print,text="";
print,text="";

!! Make a twiss of the flat machine and check wether the optics is  matched as expected
on_x1=0;on_x5=0;on_x2=0;on_x8=0;on_sep1=0;on_sep2=0;on_sep5=0;on_sep8=0;on_alice=0;on_lhcb=0;
if (mylhcbeam==1){ use,sequence=lhcb1; } else { use,sequence=lhcb2; };
twiss;
Value, table(summ,q1),table(summ,q2),table(summ,dq1),table(summ,dq2);
Value, table(twiss,IP1,betx),table(twiss,IP2,betx),table(twiss,IP5,betx),table(twiss,IP8,betx);


!! Slice
Option, -echo,-warn,-info;
slicefactor=4;
select, flag=makethin, clear;
select, flag=makethin, class=MB,    slice= 2;
select, flag=makethin, class=MQ,    slice=2 * slicefactor;
select, flag=makethin, class=mqxa,  slice=32* slicefactor;
select, flag=makethin, class=mqxb, slice=32* slicefactor;
select, flag=makethin, pattern=mbx\. ,   slice=4;
select, flag=makethin, pattern=mbrb\.,   slice=4;
select, flag=makethin, pattern=mbrc\.,   slice=4;
select, flag=makethin, pattern=mbrs\.,   slice=4;
select, flag=makethin, pattern=mqwa\.,   slice=4;
select, flag=makethin, pattern=mqwb\.,   slice=4;
select, flag=makethin, pattern=mqy\.,    slice=4* slicefactor;
select, flag=makethin, pattern=mqm\.,    slice=4* slicefactor;
select, flag=makethin, pattern=mqmc\.,   slice=4* slicefactor;
select, flag=makethin, pattern=mqml\.,   slice=4* slicefactor;
select, flag=makethin, pattern=mqtlh\.,  slice=2* slicefactor;
select, flag=makethin, pattern=mqtli\.,  slice=2* slicefactor;
select, flag=makethin, pattern=mqt\.  ,  slice=2* slicefactor;
if (mylhcbeam==1){
  use,sequence=lhcb1;
  makethin, sequence=lhcb1, makedipedge=false, style=teapot, makeendmarkers=true;
 } else {
  use,sequence=lhcb2;
  makethin, sequence=lhcb2, makedipedge=false, style=teapot, makeendmarkers=true;
};

! Add more aperture markers (moved to after slicing to avoid negative drifts)
!if (mylhcbeam<4){
!  call,   file="run3aper/aperture_YETS2021_2021-11-10.seq";
!} else {
!  call,   file="run3aper/aperture_YETS2021_2021-11-10_b4.seq";
!};

!! Use sequence (has to be set again after slicing but before errors/misalignments)
if (mylhcbeam==1){ use,sequence=lhcb1; } else { use,sequence=lhcb2; };

!! Align separation magnets
!call,file="dbrun3/toolkit/align_sepdip.madx";
!exec, align_mbxw;
!exec, align_mbrc15;
!exec, align_mbx28;
!exec, align_mbrc28;
!exec, align_mbrs;
!exec, align_mbrb;


!##################
!# Machine tuning #
!##################

!! Octupole Settings
brho:=NRJ*1e9/clight;
if (mylhcbeam==1){
  KOF.A12B1:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A23B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOF.A34B1:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A45B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOF.A56B1:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A67B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOF.A78B1:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A81B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A12B1:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A23B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A34B1:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A45B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A56B1:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A67B1:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A78B1:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A81B1:=Kmax_MO*I_MO/Imax_MO/brho;
};
if (mylhcbeam>1){
  KOF.A12B2:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A23B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOF.A34B2:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A45B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOF.A56B2:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A67B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOF.A78B2:=Kmax_MO*I_MO/Imax_MO/brho; KOF.A81B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A12B2:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A23B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A34B2:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A45B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A56B2:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A67B2:=Kmax_MO*I_MO/Imax_MO/brho;
  KOD.A78B2:=Kmax_MO*I_MO/Imax_MO/brho; KOD.A81B2:=Kmax_MO*I_MO/Imax_MO/brho;
};


!! Activate crossing
on_x1=on_x1_aux; on_x5=on_x5_aux; on_x2=on_x2_aux; on_x8=on_x8_aux; on_sep1=on_sep1_aux; on_sep2=on_sep2_aux; on_sep5=on_sep5_aux; on_sep8=on_sep8_aux; on_alice=on_alice_aux; on_lhcb=on_lhcb_aux;

if(on_collision==1){
  !! Remove separation (we are simulating collisions)
  on_sep1=0; on_sep5=0; on_sep2=0; on_sep8=0;
};

!! Match tune and chroma
if(mylhcbeam==1){
  kqtf=dQx.b1_sq;kqtd=dQy.b1_sq;  dQx.b1_sq:=kqtf;dQy.b1_sq:=kqtd;
  ksf=dQpx.b1_sq;ksd=dQpy.b1_sq;  dQpx.b1_sq:=ksf;dQpy.b1_sq:=ksd;
};
if(mylhcbeam>1){
  kqtf=dQx.b2_sq;kqtd=dQy.b2_sq;  dQx.b2_sq:=kqtf;dQy.b2_sq:=kqtd;
  ksf=dQpx.b2_sq;ksd=dQpy.b2_sq;  dQpx.b2_sq:=ksf;dQpy.b2_sq:=ksd;
};

match;
global, q1=qx0, q2=qy0;
vary,   name=kqtf, step=1.0E-7 ;
vary,   name=kqtd, step=1.0E-7 ;
lmdif,  calls=100, tolerance=1.0E-21;
endmatch;

match,chrom;
global, dq1=qprime, dq2=qprime;
vary,   name=ksf;
vary,   name=ksd;
lmdif,  calls=100, tolerance=1.0E-21;
endmatch;

match,chrom;
global, dq1=qprime, dq2=qprime;
global, q1=qx0, q2=qy0;
vary,   name=ksf;
vary,   name=ksd;
vary,   name=kqtf, step=1.0E-7 ;
vary,   name=kqtd, step=1.0E-7 ;
lmdif,  calls=500, tolerance=1.0E-21;
endmatch;

!! RF cavities on
VRF400=12;  !MV
LAGRF400.b1=0.5;
LAGRF400.b2=0.5;

!#################
!# Create Output #
!#################

select,flag=twiss,clear;
select,flag=twiss,class=collimator,column=name,s,L,betx,bety,alfx,alfy,mux,muy,x,y,px,py,dx,dy,dpx,dpy;
select,flag=twiss,class=rcollimator,column=name,s,L,betx,bety,alfx,alfy,mux,muy,x,y,px,py,dx,dy,dpx,dpy;
twiss,file="out/collimators_B1.tfs";

select,flag=twiss,clear;
select,flag=twiss,column=name,keyword,s,L,betx,bety,alfx,alfy,mux,muy,x,y,px,py,dx,dy,dpx,dpy,apertype,mech_sep;
twiss,file="out/all_optics_B1.tfs";

set,format="20.14f";
value,table(twiss,IP1,betx),table(twiss,IP1,bety),table(twiss,IP5,betx),table(twiss,IP5,bety),table(twiss,IP2,betx),table(twiss,IP2,bety),table(twiss,IP8,betx);
value,table(twiss,IP7,x)*1e6,table(twiss,IP7,y)*1e6,table(twiss,IP1,x),table(twiss,IP1,py),table(twiss,IP5,y),table(twiss,IP5,px);

! sixtrack, radius=17E-03, cavall, aperture=true, long_names=true;
sixtrack, radius=17E-03, cavall;

system,"mv fc.2 out/b1/fc.2";
system,"mv fc.3 out/b1/fc.3";
!system,"mv fc.3.aper out/b1/fc3.limi";
system,"mv fc.3.aux out/b1/fc.3.aux";
system,"mv fc.34 out/b1/fc.34";
system,"mv fc.8 out/b1/fc.8";
system,"mv fc.16 out/b1/fc.16";

system,"rm -rf dbrun3";
system,"rm -rf run3opt";

return;



