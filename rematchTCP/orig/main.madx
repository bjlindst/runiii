title,"lhc6.5 as-built collision thick optics (7 TeV)";

set,    format="27.17f";
option, -echo, -info;
system,"ln -fns /afs/cern.ch/eng/lhc/optics/runII/2017 db2017";
system,"ln -fns /afs/cern.ch/eng/lhc/optics/runII/2016 db5";
system,"ln -fns /afs/cern.ch/eng/lhc/optics/runII/2018/PROTON ats";
System,"ln -fns /afs/cern.ch/eng/lhc/optics/runII/2018/ db2018";
System,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII/RunIII_dev dbrun3";
System,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII/RunIII_dev/2021/ db2021";
System,"ln -fns /afs/cern.ch/eng/lhc/optics/runIII/RunIII_dev/2022/ db2022";

! for vss markers
 REAL CONST L.TANAL    = 0.0;
 REAL CONST L.TANAR    = 0.0;
 REAL CONST L.TANC     = 0.0;
 REAL CONST L.TCDQA    = 0.0;
 REAL CONST L.TCP      = 0.0;
 REAL CONST L.MBAS2    = 0.0;
 REAL CONST L.MBCS2    = 0.0;
 REAL CONST L.MBLS2    = 0.0;
 REAL CONST L.TASB     = 0.0;
 REAL CONST L.BTVSS075 = 0.0;
 REAL CONST L.TCTVB    = 0.0;
 REAL CONST L.X5ZDC002 = 0.0;
 REAL CONST L.TCDDM    = 0.0;
 REAL CONST L.ACNCA    = 0.0;
 REAL CONST l.LEJL     = 0.0;
 REAL CONST l.MBLW     = 0.0;
 REAL CONST l.TCSM     = 0.0;

on_holdselect=0;     ! TODO comment
on_fixedip=0;        ! TODO comment
match_on_aperture=0; ! TODO comment

eps=2.0e-6/7460.52; ! emittance for 7TeV ! TODO why 2 µm?

! nominal sequence
call,file="db2018/lhc_as-built.seq";
call,file="dbrun3/IR7-Run3seqedit.madx";  !! New IR7 MQW layout and cabling

nrj=7000;  !nrj=450;
Beam,particle=proton,sequence=lhcb1,energy=NRJ,NPART=1.15E11,sige=4.5e-4;
Beam,particle=proton,sequence=lhcb2,energy=NRJ,bv = -1,NPART=1.15E11,sige=4.5e-4;         

!! Load some optics
call,file="db2021/PROTON/opticsfile.21";     !! 2021 optics, 80 cm
!call,file="db2022/PROTON/opticsfile.34";    !! 2022 optics, 20 cm

call,file="/afs/cern.ch/eng/lhc/optics/HLLHCV1.0/toolkit/macro.madx";

! to switch on/off resp. crossing angles/separation/experiments dipoles at IPs (those numbers
! are strength, not only switches)
 on_x1:=0; on_sep1:=0; on_atlas  :=  0 ; on_sol_atlas  :=  0 ;
 on_x2:=0; on_sep2:=0; on_alice  :=  0 ; on_sol_alice  :=  0 ;
 on_x5:=0; on_sep5:=0; on_cms    :=  0 ; on_sol_cms    :=  0 ;
 on_x8:=0; on_sep8:=0; on_lhcb   :=  0 ;

! first twiss
set,format="27.17f";
use,period=lhcb1;
select,flag=twiss,column=name,keyword,s,l,betx,bety,alfx,alfy,dx,dpx,x,px,y,py,mux,muy,ANGLE,K1L;
!savebeta, label=bir7b1, place=s.ds.l7.b1, sequence=lhcb1;
twiss,file=twiss_initial_b1.tfs;
use,period=lhcb2;
twiss,file=twiss_initial_b2.tfs;

imp(betax,betay,len,angle): macro = {
    !value,angle,eps,len,betax,betay;
    ! compute sigma and impedance scaling factors for the collimators
    sigma=sqrt(eps*(cos(angle)*cos(angle)*betax + sin(angle)*sin(angle)*betay));
    impx=len*betax*(cos(angle)*cos(angle)+sin(angle)*sin(angle)/2)/(sigma*sigma*sigma);
    impy=len*betay*(sin(angle)*sin(angle)+cos(angle)*cos(angle)/2)/(sigma*sigma*sigma);
    !value,impx,impy;
};


if(on_holdselect==0){ exec,select(7,67,78,b1); }; ! TODO what does this do?
value,bir7b1->betx; ! 33.45581281595753609
use,sequence=lhcb1,range=s.ds.l7.b1/e.ds.r7.b1;
twiss, beta0=bir7b1, file=twiss_ir7_initial_B1.tfs; ! TODO bir7b1 where is this set??
! impedance using coefficients for CFC collimators
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

imptotalb1: macro={
    !use,sequence=lhcb1;
    use,sequence=lhcb1,range=s.ds.l7.b1/e.ds.r7.b1;
    twiss, beta0=bir7b1;
    
    imptotx=0;imptoty=0;
    bx=table(twiss,TCP.D6L7.B1,betx);by=table(twiss,TCP.D6L7.B1,bety);exec,imp(bx,by,1.32,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.C6L7.B1,betx);by=table(twiss,TCP.C6L7.B1,bety);exec,imp(bx,by,1.32,0);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.B6L7.B1,betx);by=table(twiss,TCP.B6L7.B1,bety);exec,imp(bx,by,1.32,2.22);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A6L7.B1,betx);by=table(twiss,TCSG.A6L7.B1,bety);exec,imp(bx,by,1,2.46);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5L7.B1,betx);by=table(twiss,TCSG.B5L7.B1,bety);exec,imp(bx,by,1,2.5);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A5L7.B1,betx);by=table(twiss,TCSG.A5L7.B1,bety);exec,imp(bx,by,1,0.71);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.D4L7.B1,betx);by=table(twiss,TCSG.D4L7.B1,bety);exec,imp(bx,by,1,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B4L7.B1,betx);by=table(twiss,TCSG.B4L7.B1,bety);exec,imp(bx,by,1,0);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4L7.B1,betx);by=table(twiss,TCSG.A4L7.B1,bety);exec,imp(bx,by,1,2.35);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4R7.B1,betx);by=table(twiss,TCSG.A4R7.B1,bety);exec,imp(bx,by,1,0.808);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5R7.B1,betx);by=table(twiss,TCSG.B5R7.B1,bety);exec,imp(bx,by,1,2.47);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.D5R7.B1,betx);by=table(twiss,TCSG.D5R7.B1,bety);exec,imp(bx,by,1,0.897);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.E5R7.B1,betx);by=table(twiss,TCSG.E5R7.B1,bety);exec,imp(bx,by,1,2.28);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.6R7.B1,betx);by=table(twiss,TCSG.6R7.B1,bety);exec,imp(bx,by,1,0.00873);imptotx = impx+imptotx;imptoty = impy+imptoty;
    imptotxb1=imptotx/1e13;imptotyb1=imptoty/1e13;
    !value,imptotxb1,imptotyb1;
}

exec,imptotalb1;
imptotxb1ini=imptotxb1;imptotyb1ini=imptotyb1;
value,imptotxb1ini,imptotyb1ini;

! impedance using coefficients for low-impedance collimators
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

imptotalb1LOW: macro={
    use,sequence=lhcb1,range=s.ds.l7.b1/e.ds.r7.b1;
    twiss, beta0=bir7b1;
    ! Note: To account for the smaller gap of the TCPs than TCSGs, its length is changed.
    ! With n_TCP=5 sigma, n_TCS=6.5 sigma, the TCP impedance is a factor (6.5/5)^3 higher for the same length. 
    ! The real length is 0.6m instead of 1m, so the effective TCP length becomes 0.6*(6.5/5)^3=1.32 m
    
    ! In addition, modify the effective lengths to account for MoGr and coating. 
    ! Email from Nicolas Mounet 13/1/2021: For Mo coating, scale down impedance by factor 0.1. For uncoated MoGr, scale down by 0.45
    ! Collimators with Mo coating: TCSG.D4L7.B1, TCSG.B4L7.B1, TCSG.E5R7.B1, TCSG.6R7.B1 --> new length is 0.1 instead of 1
    ! Collimators MoGr: TCP.C6L7.B1, TCP.D6L7.B1 --> new length is 1.32 (from above) * 0.45 = 0.59
    imptotx=0;imptoty=0;
    bx=table(twiss,TCP.D6L7.B1,betx);by=table(twiss,TCP.D6L7.B1,bety);exec,imp(bx,by,0.59,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty; ! MoGr
    bx=table(twiss,TCP.C6L7.B1,betx);by=table(twiss,TCP.C6L7.B1,bety);exec,imp(bx,by,0.59,0);imptotx = impx+imptotx;imptoty = impy+imptoty;	     ! MoGr
    bx=table(twiss,TCP.B6L7.B1,betx);by=table(twiss,TCP.B6L7.B1,bety);exec,imp(bx,by,1.32,2.22);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A6L7.B1,betx);by=table(twiss,TCSG.A6L7.B1,bety);exec,imp(bx,by,1,2.46);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5L7.B1,betx);by=table(twiss,TCSG.B5L7.B1,bety);exec,imp(bx,by,1,2.5);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A5L7.B1,betx);by=table(twiss,TCSG.A5L7.B1,bety);exec,imp(bx,by,1,0.71);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.D4L7.B1,betx);by=table(twiss,TCSG.D4L7.B1,bety);exec,imp(bx,by,0.1, 1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty;  ! Mo coating
    bx=table(twiss,TCSG.B4L7.B1,betx);by=table(twiss,TCSG.B4L7.B1,bety);exec,imp(bx,by,0.1, 0);imptotx = impx+imptotx;imptoty = impy+imptoty;	! Mo coating
    bx=table(twiss,TCSG.A4L7.B1,betx);by=table(twiss,TCSG.A4L7.B1,bety);exec,imp(bx,by,1,2.35);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4R7.B1,betx);by=table(twiss,TCSG.A4R7.B1,bety);exec,imp(bx,by,1,0.808);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5R7.B1,betx);by=table(twiss,TCSG.B5R7.B1,bety);exec,imp(bx,by,1,2.47);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.D5R7.B1,betx);by=table(twiss,TCSG.D5R7.B1,bety);exec,imp(bx,by,1,0.897);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.E5R7.B1,betx);by=table(twiss,TCSG.E5R7.B1,bety);exec,imp(bx,by,0.1, 2.28);imptotx = impx+imptotx;imptoty = impy+imptoty;    ! Mo coating
    bx=table(twiss,TCSG.6R7.B1,betx);by=table(twiss,TCSG.6R7.B1,bety);exec,imp(bx,by,0.1,0.00873);imptotx = impx+imptotx;imptoty = impy+imptoty;   	! Mo coating
    imptotxb1LOW=imptotx/1e13;imptotyb1LOW=imptoty/1e13;
    !value,imptotxb1,imptotyb1;
}

exec,imptotalb1LOW;
imptotxb1iniLOW=imptotxb1LOW;imptotyb1iniLOW=imptotyb1LOW;
value,imptotxb1iniLOW,imptotyb1iniLOW;

if(on_holdselect==0){ exec,select(7,67,78,b2); }; ! TODO what does this do?

use,sequence=lhcb2,range=s.ds.l7.b2/e.ds.r7.b2;
twiss, beta0=bir7b2, file=twiss_ir7_initial_B2.tfs;

! impedance using coefficients for CFC collimators
imptotalb2: macro={
    use,sequence=lhcb2,range=s.ds.l7.b2/e.ds.r7.b2;
    twiss, beta0=bir7b2;
    imptotx=0;imptoty=0;
    bx=table(twiss,TCSG.6L7.B2,betx);by=table(twiss,TCSG.6L7.B2,bety);exec,imp(bx,by,1,0.00873);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.E5L7.B2,betx);by=table(twiss,TCSG.E5L7.B2,bety);exec,imp(bx,by,1,2.28);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.D5L7.B2,betx);by=table(twiss,TCSG.D5L7.B2,bety);exec,imp(bx,by,1,0.897);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5L7.B2,betx);by=table(twiss,TCSG.B5L7.B2,bety);exec,imp(bx,by,1,2.47);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4L7.B2,betx);by=table(twiss,TCSG.A4L7.B2,bety);exec,imp(bx,by,1,0.735);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4R7.B2,betx);by=table(twiss,TCSG.A4R7.B2,bety);exec,imp(bx,by,1,2.31);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B4R7.B2,betx);by=table(twiss,TCSG.B4R7.B2,bety);exec,imp(bx,by,1,0);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.D4R7.B2,betx);by=table(twiss,TCSG.D4R7.B2,bety);exec,imp(bx,by,1,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A5R7.B2,betx);by=table(twiss,TCSG.A5R7.B2,bety);exec,imp(bx,by,1,0.71);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5R7.B2,betx);by=table(twiss,TCSG.B5R7.B2,bety);exec,imp(bx,by,1,2.51);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A6R7.B2,betx);by=table(twiss,TCSG.A6R7.B2,bety);exec,imp(bx,by,1,2.47);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.B6R7.B2,betx);by=table(twiss,TCP.B6R7.B2,bety);exec,imp(bx,by,1.32,2.23);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.C6R7.B2,betx);by=table(twiss,TCP.C6R7.B2,bety);exec,imp(bx,by,1.32,0);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.D6R7.B2,betx);by=table(twiss,TCP.D6R7.B2,bety);exec,imp(bx,by,1.32,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty;
    imptotxb2=imptotx/1e13;imptotyb2=imptoty/1e13;
    !value,imptotxb2,imptotyb2;
};

exec,imptotalb2;
imptotxb2ini=imptotxb2;imptotyb2ini=imptotyb2;
value,imptotxb2ini,imptotyb2ini;

! impedance with coefficients for low-impedance collimators
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
imptotalb2LOW: macro={
    use,sequence=lhcb2,range=s.ds.l7.b2/e.ds.r7.b2;
    twiss, beta0=bir7b2;
    ! Note: To account for the smaller gap of the TCPs than TCSGs, its length is changed.
    ! With n_TCP=5 sigma, n_TCS=6.5 sigma, the TCP impedance is a factor (6.5/5)^3 higher for the same length. 
    ! The real length is 0.6m instead of 1m, so the effective TCP length becomes 0.6*(6.5/5)^3=1.32 m
    
    ! In addition, modify the effective lengths to account for MoGr and coating. 
    ! Email from Nicolas Mounet 13/1/2021: For Mo coating, scale down impedance by factor 0.1. For uncoated MoGr, scale down by 0.45
    ! Collimators with Mo coating: TCSG.D4L7.B1, TCSG.B4L7.B1, TCSG.E5R7.B1, TCSG.6R7.B1 --> new length is 0.1 instead of 1, corresponding for B2
    ! Collimators MoGr: TCP.C6L7.B1, TCP.D6L7.B1 --> new length is 1.32 (from above) * 0.45 = 0.59
    imptotx=0;imptoty=0;
    bx=table(twiss,TCSG.6L7.B2,betx);by=table(twiss,TCSG.6L7.B2,bety);exec,imp(bx,by,0.1,0.00873);imptotx = impx+imptotx;imptoty = impy+imptoty;    ! Mo coating
    bx=table(twiss,TCSG.E5L7.B2,betx);by=table(twiss,TCSG.E5L7.B2,bety);exec,imp(bx,by,0.1,2.28);imptotx = impx+imptotx;imptoty = impy+imptoty;     ! Mo coating
    bx=table(twiss,TCSG.D5L7.B2,betx);by=table(twiss,TCSG.D5L7.B2,bety);exec,imp(bx,by,1,0.897);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B5L7.B2,betx);by=table(twiss,TCSG.B5L7.B2,bety);exec,imp(bx,by,1,2.47);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4L7.B2,betx);by=table(twiss,TCSG.A4L7.B2,bety);exec,imp(bx,by,1,0.735);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A4R7.B2,betx);by=table(twiss,TCSG.A4R7.B2,bety);exec,imp(bx,by,1,2.31);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.B4R7.B2,betx);by=table(twiss,TCSG.B4R7.B2,bety);exec,imp(bx,by,0.1,0);imptotx = impx+imptotx;imptoty = impy+imptoty;        ! Mo coating
    bx=table(twiss,TCSG.D4R7.B2,betx);by=table(twiss,TCSG.D4R7.B2,bety);exec,imp(bx,by,0.1,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty;   ! Mo coating
    bx=table(twiss,TCSG.A5R7.B2,betx);by=table(twiss,TCSG.A5R7.B2,bety);exec,imp(bx,by,1,0.71);imptotx = impx+imptotx;imptoty = impy+imptoty;     
    bx=table(twiss,TCSG.B5R7.B2,betx);by=table(twiss,TCSG.B5R7.B2,bety);exec,imp(bx,by,1,2.51);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCSG.A6R7.B2,betx);by=table(twiss,TCSG.A6R7.B2,bety);exec,imp(bx,by,1,2.47);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.B6R7.B2,betx);by=table(twiss,TCP.B6R7.B2,bety);exec,imp(bx,by,1.32,2.23);imptotx = impx+imptotx;imptoty = impy+imptoty;
    bx=table(twiss,TCP.C6R7.B2,betx);by=table(twiss,TCP.C6R7.B2,bety);exec,imp(bx,by,0.59,0);imptotx = impx+imptotx;imptoty = impy+imptoty;      ! MoGr
    bx=table(twiss,TCP.D6R7.B2,betx);by=table(twiss,TCP.D6R7.B2,bety);exec,imp(bx,by,0.59,1.5708);imptotx = impx+imptotx;imptoty = impy+imptoty; ! MoGr
    imptotxb2LOW=imptotx/1e13;imptotyb2LOW=imptoty/1e13;
    !value,imptotxb2,imptotyb2;
};

exec,imptotalb2LOW;
imptotxb2iniLOW=imptotxb2LOW;imptotyb2iniLOW=imptotyb2LOW;
value,imptotxb2iniLOW,imptotyb2iniLOW;

!! Load strengths from final matching
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
call,file="ir7_new.str";
set,    format="27.17f";
select, flag=twiss, clear;
select, flag=twiss, column=name,keyword,s,betx,bety,x,px,y,py,mux,muy,alfx,alfy,dx,dpx;
use, period=lhcb1;
twiss, file=twiss_final_b1.tfs;
use, period=lhcb2;
twiss, file=twiss_final_b2.tfs;

exec,imptotalb1;
imptotxb1final=imptotxb1; imptotyb1final=imptotyb1; 
exec,imptotalb2;
imptotxb2final=imptotxb2; imptotyb2final=imptotyb2; 

value, imptotxb1ini, imptotxb1final,imptotyb1ini, imptotyb1final;
value, imptotxb2ini, imptotxb2final,imptotyb2ini, imptotyb2final;
value, imptotxb1final/imptotxb1ini;
value, imptotyb1final/imptotyb1ini;
value, imptotxb2final/imptotxb2ini;
value, imptotyb2final/imptotyb2ini;

exec,imptotalb1LOW;
imptotxb1finalLOW=imptotxb1LOW; imptotyb1finalLOW=imptotyb1LOW; 
exec,imptotalb2LOW;
imptotxb2finalLOW=imptotxb2LOW; imptotyb2finalLOW=imptotyb2LOW; 

value, imptotxb1iniLOW, imptotxb1finalLOW,imptotyb1iniLOW, imptotyb1finalLOW;
value, imptotxb2iniLOW, imptotxb2finalLOW,imptotyb2iniLOW, imptotyb2finalLOW;
value, imptotxb1finalLOW/imptotxb1iniLOW;
value, imptotyb1finalLOW/imptotyb1iniLOW;
value, imptotxb2finalLOW/imptotxb2iniLOW;
value, imptotyb2finalLOW/imptotyb2iniLOW;

system, "rm db* db2017 ats";
